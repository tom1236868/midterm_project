const functions = require('firebase-functions');

const admin = require('firebase-admin');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.addTime = functions.database.ref('/posts/{pushId}')
    .onCreate((snapshot, context) => {
      const Time = admin.database.ServerValue.TIMESTAMP;
      return snapshot.ref.child('time').set(Time);
    });
exports.updateTime = functions.database.ref('/posts/{pushId}')
  .onWrite((change) => {
    const Time = admin.database.ServerValue.TIMESTAMP;
    const After = change.after;
    return After.ref.child('time').set(Time);
  });
