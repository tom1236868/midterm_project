function init() {
    var user_email = '';
    var user_name = '';
    var user_pic = '';
    var UserRef = firebase.database().ref('Users');
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('navmenu');
        var dropdown = document.getElementById('dropdown');
        var User = document.getElementById('username');
        var Pic = document.getElementById('Pic');
        // Check user login
        if (user) {
            user_email = user.email;
            if(user.photoURL != null) user_pic = user.photoURL;
            else user_pic = "../img/guset.png";
            UserRef.once('value', function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    if(childSnapshot.val().email == user_email)
                    {
                        user_name = childSnapshot.val().username;
                        console.log(user_name);
                        menu.innerHTML = "<img src='" + user_pic + "' alt='User Profile' class='rounded-circle' style='width:40px;'>" + ' ' + user_name;
                        Pic.innerHTML = "<img src='" + user_pic + "' class='rounded' alt='user icon' id='user_icon'> <h3 id='username'>" + user_name+"</h3>";
                    }
                        
                });
            }).catch(e => console.log(e.message));

            dropdown.innerHTML = "<a class='dropdown-item' href='userpage.html'>User Page</a><a class='dropdown-item' href='#' id='logout-btn'>Logout</a>";
            
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            
            var btnLogout = document.getElementById("logout-btn");

            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                    alert("Logout Success!");
                  }).catch(function(error) {
                    // An error happened.
                    alert("Logout Failed!");
                  });
            })
            var URL = document.getElementById('url')
            var pic_btn = document.getElementById('upload_button');
            pic_btn.addEventListener('click', function(){
                if(URL.value != "")
                {
                    var user = firebase.auth().currentUser;
                    user.updateProfile({
                        photoURL: URL.value
                      }).then(function() {
                        window.location.href = "userpage.html";
                      }).catch(function(error) {
                        // An error happened.
                      });
                }
            })

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<img src='../img/guset.png' alt='User Profile' class='rounded-circle' style='width:40px;'>" + 'Guest';
            dropdown.innerHTML = "<a class='dropdown-item' href='signin.html'>sign in</a>";
            postbutton.innerHTML = "";
        }
    });

}

window.onload = function () {
    init();
};