function init() {
    var user_email = '';
    var user_name = '';
    var user_pic = '';
    var UserRef = firebase.database().ref('Users');
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('navmenu');
        var dropdown = document.getElementById('dropdown');
        var postbutton = document.getElementById('btn')
        // Check user login
        if (user) {
            user_email = user.email;
            if(user.photoURL != null) user_pic = user.photoURL;
            else user_pic = "../img/guset.png";
            console.log(user_email);
            console.log(user_pic);
            UserRef.once('value', function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    if(childSnapshot.val().email == user_email)
                    {
                        user_name = childSnapshot.val().username;
                        console.log(user_name);
                        menu.innerHTML = "<img src='" + user_pic + "' alt='User Profile' class='rounded-circle' style='width:40px;'>" + ' ' + user_name;
                    }
                        
                });
            }).catch(e => console.log(e.message));

            dropdown.innerHTML = "<a class='dropdown-item' href='userpage.html'>User Page</a><a class='dropdown-item' href='#' id='logout-btn'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            
            var btnLogout = document.getElementById("logout-btn");

            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                    alert("Logout Success!");
                  }).catch(function(error) {
                    // An error happened.
                    alert("Logout Failed!");
                  });
            })

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<img src='../img/guset.png' alt='User Profile' class='rounded-circle' style='width:40px;'>" + 'Guest';
            dropdown.innerHTML = "<a class='dropdown-item' href='signin.html'>sign in</a>";
            postbutton.innerHTML = "";
        }
    });

    // The html code for post
    var str_before_title = "<tr><td><a class='posts' href='";
    var str_after_title = "</a></td><td>"
    var str_before_time = "</td><td>"
    var str_after_time = "</td></tr>"

    var postsRef = firebase.database().ref('/posts');
    // List for store posts html
    var total_post = [];

    postsRef.on('child_added', function(data){
        var time = new Date(data.val().time);
        time.toLocaleString();
        if(data.val().group == 'A')
        total_post.push(str_before_title + "#" + "'>" + data.val().title + str_after_title + data.val().author + str_before_time + time + str_after_time);
        document.getElementById('post_list').innerHTML = total_post.join('');
        //window.location.href = "index.html";
        var post_link = document.getElementsByClassName('posts');
        for(i=0;i<post_link.length;i++)
        {
            post_link[i].addEventListener('click', function(){
                var str_bef_title = "<a class='btn btn-info' id='back_button' href='../index.html' >回到上一頁</a><h1 id='title'>";
                var str_before_pic = "</h1><hr><div class='row' id='postcontent'><div class='col-md-3'><img src='";
                var str_before_user = "' alt='User Profile' id='usericon'><p><label for='usericon'>";
                var str_before_content = "</label></div><div class='col-md-9'><p>"
                var str_after_content = "</p></div></div><hr></hr>";
                var post_content = [];
                var name = this.innerHTML;
                var postkey;
                postsRef.once('value',function (snapshot) {
                    snapshot.forEach(function(childSnapshot){
                        var Name = childSnapshot.val().title;
                        var author_pic = childSnapshot.val().profile_photo;
                        var author = childSnapshot.val().author;
                        var content = childSnapshot.val().content;
                        if(Name == name)
                        {
                            post_content.push(str_bef_title + Name + str_before_pic + author_pic + str_before_user + author + str_before_content + content + str_after_content);
                            var commentRef = childSnapshot.child('comments');
                            commentRef.forEach(function(com){
                                var str_b_pic = "<div class='row' id='postcontent'><div class='col-md-3'><img src='";
                                var str_b_user = "' alt='User Profile' id='usericon'><p><label for='usericon'>";
                                var str_b_comment = "</label></div><div class='col-md-9'><p>";
                                var str_a_comment = "</p></div></div><hr></hr>";
                                if(com.key != "default" && com.val().group == 'A')
                                    post_content.push(str_b_pic + com.val().profile_photo + str_b_user + com.val().author + str_b_comment + com.val().comment + str_a_comment);
                            })
                            postkey = childSnapshot.key;
                        }
                    });
                    document.getElementById('main').innerHTML = post_content.join('') + "<div class='form-group'><h4>Reply to this post :</h4><textarea class='form-control' rows='5' id='comment'></textarea></div><button type='button' class='btn-lg btn-info btn-block' id='reply_button'>Send</button> </div>";
                    //window.location.href = "index.html";
                    var comment_btn = document.getElementById('reply_button');
                    comment_btn.addEventListener('click', function(){
                        var comment_txt = document.getElementById('comment');
                        if(comment_txt.value != "")
                        {
                            var postRef = firebase.database().ref('posts/' + postkey + "/comments");
                            postRef.push().set({
                                author : user_name,
                                email : user_email,
                                comment : comment_txt.value,
                                profile_photo : user_pic,
                                group : 'A'
                            })
                            window.location.href = "../index.html";
                        }
                    })
                })
                .catch(e => console.log(e.message));
            })
        }
    });

}

window.onload = function () {
    init();
};