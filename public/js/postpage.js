function init() {
    var user_email = '';
    var user_name = '';
    var user_pic = '';
    var UserRef = firebase.database().ref('Users');
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('navmenu');
        var dropdown = document.getElementById('dropdown');
        // Check user login
        if (user) {
            user_email = user.email;
            if(user.photoURL != null) user_pic = user.photoURL;
            else user_pic = "../img/guset.png";
            UserRef.once('value', function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    if(childSnapshot.val().email == user_email)
                    {
                        user_name = childSnapshot.val().username;
                        menu.innerHTML = "<img src='" + user_pic + "' alt='User Profile' class='rounded-circle' style='width:40px;'>" + ' ' + user_name;
                    }
                        
                });
            }).catch(e => console.log(e.message));

            dropdown.innerHTML = "<a class='dropdown-item' href='userpage.html'>User Page</a><a class='dropdown-item' href='#' id='logout-btn'>Logout</a>";
            
            var btnLogout = document.getElementById("logout-btn");

            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                    alert("Logout Success!");
                  }).catch(function(error) {
                    // An error happened.
                    alert("Logout Failed!");
                  });
            })

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<img src='" + user_pic + "' alt='User Profile' class='rounded-circle' style='width:40px;'>" + ' ' + user_name;
            dropdown.innerHTML = "<a class='dropdown-item' href='html/signin.html'>sign in</a>";
        }
    });

    post_btn = document.getElementById('post_button');
    post_title = document.getElementById('title')
    post_txt = document.getElementById('content');

    var postsRef = firebase.database().ref('posts');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "" && post_title != "") {
            postsRef.push().set({
                author : user_name,
                email : user_email,
                title : post_title.value,
                content : post_txt.value,
                profile_photo : user_pic,
                group : 'A',
                comments : 
                {
                    default:{
                        author : '',
                        email : '',
                        comment : '',
                        profile_photo : '',
                        group : ''
                    }
                }
            })
            create_alert("success","Upload success!")
            window.location.href = "../index.html";
        }
    });

    function create_alert(type, message) {
        var alertarea = document.getElementById('custom-alert');
        if (type == "success") {
            str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
        } else if (type == "error") {
            str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
        }
    }
}

window.onload = function () {
    init();
};