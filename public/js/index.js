function init() {
    var user_email = '';
    var user_name = '';
    var user_pic = '';
    var UserRef = firebase.database().ref('Users');
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('navmenu');
        var dropdown = document.getElementById('dropdown');
        var postbutton = document.getElementById('btn')
        // Check user login
        if (user) {
            user_email = user.email;
            if(user.photoURL != null) user_pic = user.photoURL;
            else user_pic = 'img/guset.png';
            console.log(user_email)
            UserRef.once('value', function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    console.log(childSnapshot.val().email);
                    if(childSnapshot.val().email == user_email)
                    {
                        user_name = childSnapshot.val().username;
                        console.log(user_name);
                        menu.innerHTML = "<img src='" + user_pic + "' alt='User Profile' class='rounded-circle' style='width:40px;'>" + ' ' + user_name;
                    }
                        
                });
            }).catch(e => console.log(e.message));

            dropdown.innerHTML = "<a class='dropdown-item' href='html/userpage.html'>User Page</a><a class='dropdown-item' href='#' id='logout-btn'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            
            var btnLogout = document.getElementById("logout-btn");

            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                    alert("Logout Success!");
                  }).catch(function(error) {
                    // An error happened.
                    alert("Logout Failed!");
                  });
            })

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<img src='img/guset.png' alt='User Profile' class='rounded-circle' style='width:40px;'> Guset";
            dropdown.innerHTML = "<a class='dropdown-item' href='html/signin.html'>sign in</a>";
        }
    });

   

}

window.onload = function () {
    init();
};