# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 2019 Software studio midterm Project (Forum)
* Key functions (add/delete)
    1. 擁有動態nav bar，可顯示目前登入使用者的username及icon。
    2. 簡潔的post page 和 post list ，全部利用同一個介面，節省頁面用量。
    3. 除了email sign up/sign in ，還有google 及 facebook等方式。
    4. 善用使用者登入功能，訪客將無法留言，發文及更改icon。
    5. 大量使用bootstrap及CSS排版，保持美觀清潔及RWD的功能。
    
* Other functions (add/delete)
    1. user page可以利用URL更改icon。
    2. 運用 cloud function幫文章的最後更新時間更新，保持文章可信度。
    3. sign up介面使用簡單CSS動畫，展現設計感。

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://software-studio-midterm-92cdf.firebaseapp.com/

# Components Description : 
1. 動態nav bar :user 自訂 icon 及 username
2. post page : 可以訂定標題和內容，根據在哪個版發文。
3. 使用者登入 : 訪客無權留言，但可以閱讀文章。也無法發文及使用userpage。
4. 使用bootstrap確保RWD運作正常，輔以CSS增加版面美觀。
5. Email, google, Facebook sign in統整同一介面。 


# Other Functions Description(1~10%) : 
1. 自訂icon : user可以在user page貼上icon url，更換自己的icon。
2. CSS動畫 : 僅配布在sign up page，有簡單的淡入效果。
3. cloud function : 在文章發布的同時，幫文章加上伺服器時間戳記。


## Security Report (Optional)
安全性的部分雖然並未實作，但是可以分成幾點做討論：
1. "<input>"類標籤，這類標籤，尤其是text跟textarea，因為沒有做input檢查，所以塞進來java script的情形無法避免，如果考慮安全性的話，可以對字串做token檢查。
2. 網頁內主要大部分的Write操作都在必須登入的情況下才能使用，若沒有登入，連通向write頁面的方式都沒有。再者，避免大量使用cloud function。因為大部分的cloud function都擁有繞過 database rule的權限，所以安全性較高的操作不應通過cloud function進行。
3. google 或是 facebook sign in的部分都是用token的形式，有經過加密，不會有輕易外洩的問題。
